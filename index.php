<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      
    <title>Adeltec Soluções</title>

    <link href="css/style.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/layout/favicon.png" type="image/x-icon" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  	<header class="header">
      <div class="container">
        <a href="" class="logo">
          <img src="img/layout/logo.png" alt="Adeltec">
        </a>

        <nav class="menu">
          <ul>
            <li><a href="#" title="Institucional">Institucional</a></li>
            <li><a href="#" title="Produtos">Produtos <i class="icon-down-dir"></i></a></li>
            <li><a href="#" title="Serviços">Serviços <i class="icon-down-dir"></i></a></li>
            <li><a href="#" title="Clientes">Clientes</a></li>
            <li><a href="#" title="Blog">Blog</a></li>
            <li><a href="#" title="Contato">Contato</a></li>
          </ul>
        </nav>

        <div class="contact-info">
          <i class="icon-square icon-phone"></i>
          <a href="#">(81) 3636.3636</a>
        </div>
      </div>
    </header>
      
    <div class="banners">
      <div class="talk-to-experts">
        <form action="" method="post" class="form" id="form-experts">
          <h2>Solicite um orçamento</h2>
          <h1>Fale com os nossos especialistas</h1>
          <input type="text" name="nome" placeholder="Nome" class="form-control" />
          <input type="email" name="email" placeholder="Email" class="form-control" />
          <div class="select">
            <select name="produto-servico">
              <option>Produto / Serviço</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
            </select>
          </div>
          <button type="submit" class="btn-default">Enviar solicitação <i class="icon-arrow"></i></button>
        </form> 
      </div>

      <div class="owl-carousel">
        <div class="item">
          <img src="img/layout/banner.png" alt="Banner" />
          <div class="caption">
            <article>
              <p>Trabalhamos com soluções completas para você e sua empresa.</p>
              <a href="#" title="Nossos serviços">Nossos serviços <i class="icon-arrow"></i></a>
            </article>
          </div>
        </div>
  
        <div class="item">
          <img src="img/layout/banner.png" alt="Banner" />
          <div class="caption">
            <article>
              <p>Trabalhamos com soluções completas para você e sua empresa.</p>
              <a href="#" title="Nossos serviços">Nossos serviços <i class="icon-arrow"></i></a>
            </article>
          </div>
        </div>
      </div>
    </div>
    
    <div class="container">
      
      <div class="media-default">
        <h3>Nossos produtos</h3>

        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-hour"></i>
            <div class="media-body">
              <h4>Relógio de ponto</h4>
              <p>Empresas com mais de dez empregados devem fazer o controle da jornada de trabalho através da marcação de ponto. Geralmente através de relógios de ponto.</p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-hour"></i>
            <div class="media-body">
              <h4>Relógio de ponto</h4>
              <p>Empresas com mais de dez empregados devem fazer o controle da jornada de trabalho através da marcação de ponto. Geralmente através de relógios de ponto.</p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-hour"></i>
            <div class="media-body">
              <h4>Relógio de ponto</h4>
              <p>Empresas com mais de dez empregados devem fazer o controle da jornada de trabalho através da marcação de ponto. Geralmente através de relógios de ponto.</p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-hour"></i>
            <div class="media-body">
              <h4>Relógio de ponto</h4>
              <p>Empresas com mais de dez empregados devem fazer o controle da jornada de trabalho através da marcação de ponto. Geralmente através de relógios de ponto.</p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-hour"></i>
            <div class="media-body">
              <h4>Relógio de ponto</h4>
              <p>Empresas com mais de dez empregados devem fazer o controle da jornada de trabalho através da marcação de ponto. Geralmente através de relógios de ponto.</p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-hour"></i>
            <div class="media-body">
              <h4>Relógio de ponto</h4>
              <p>Empresas com mais de dez empregados devem fazer o controle da jornada de trabalho através da marcação de ponto. Geralmente através de relógios de ponto.</p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>
        </div>
      </div>
      <!-- Nossos produtos -->

      <div class="highlight-two">
        <section class="first">
          <div class="text">
            <h2>Manutenção<br /> de servidores</h2>
            <p>A Adeltec Soluções presta serviços de manutenção de servidores de diversas marcas, com fornecimento de peças.</p>
            <a href="#" class="btn-default">Saiba mais <i class="icon-arrow"></i></a>
          </div>
        </section>
        <img src="img/layout/foto.png" alt="">
        <section class="last">
          <div class="text">
            <h2>Cabeamento Estruturado</h2>
            <p>A Adeltec possui soluções em cabeamento estruturado, permitindo a direção e tráfego de qualquer tipo de sinal elétrico, envolvendo áudio, vídeo e dados.</p>
            <a href="#" class="btn-default">Saiba mais <i class="icon-arrow"></i></a>
          </div>
        </section>
      </div>
      <!-- destaques -->

      <div class="media-default">
        <h3>Serviços</h3>

        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-share"></i>
            <div class="media-body">
              <h4>Data center</h4>
              <p>Abrigam os sistemas mais críticos de uma rede e são vitais para a continuidade das operações diárias. Com isso, a segurança e a confiabilidade são uma prioridade para as organizações. </p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-share"></i>
            <div class="media-body">
              <h4>Data center</h4>
              <p>Abrigam os sistemas mais críticos de uma rede e são vitais para a continuidade das operações diárias. Com isso, a segurança e a confiabilidade são uma prioridade para as organizações. </p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-share"></i>
            <div class="media-body">
              <h4>Data center</h4>
              <p>Abrigam os sistemas mais críticos de uma rede e são vitais para a continuidade das operações diárias. Com isso, a segurança e a confiabilidade são uma prioridade para as organizações. </p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-share"></i>
            <div class="media-body">
              <h4>Data center</h4>
              <p>Abrigam os sistemas mais críticos de uma rede e são vitais para a continuidade das operações diárias. Com isso, a segurança e a confiabilidade são uma prioridade para as organizações. </p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-share"></i>
            <div class="media-body">
              <h4>Data center</h4>
              <p>Abrigam os sistemas mais críticos de uma rede e são vitais para a continuidade das operações diárias. Com isso, a segurança e a confiabilidade são uma prioridade para as organizações. </p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>

          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 item">
            <i class="icon-circle icon-share"></i>
            <div class="media-body">
              <h4>Data center</h4>
              <p>Abrigam os sistemas mais críticos de uma rede e são vitais para a continuidade das operações diárias. Com isso, a segurança e a confiabilidade são uma prioridade para as organizações. </p>
              <a href="#" title="Saiba mais">Saiba mais <i class="icon-arrow"></i></a>
            </div>
          </div>
        </div>
      </div>
      <!-- Serviços -->

      <div class="get-a-quote">
        <article>
          <div class="text">
            <h5>Solicite um orçamento</h5>
            <p>Serviços de manutenção e suporte téctico para busca de informações e soluções de seus problemas.</p>
          </div>
          <a href="#" title="Solicitar" class="btn-default">Solicitar <i class="icon-arrow"></i></a>
        </article>

        <img src="img/layout/person.png" alt="Solicite um orçamento">
      </div> 

    </div>

    <footer>
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <img src="img/layout/logo-footer.png" alt="Adeltc">
          </div>
          <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
            <nav>
              <ul>
                <li><a href="#" title="Institucional">Institucional</a></li>
                <li><a href="#" title="Produtos">Produtos</a></li>
                <li><a href="#" title="Serviços">Serviços</a></li>
                <li><a href="#" title="Clientes">Clientes</a></li>
                <li><a href="#" title="Blog">Blog</a></li>
                <li><a href="#" title="Contato">Contato</a></li>
              </ul>
            </nav>
          </div>
          <div class="col-xs-12 col-sm-2 col-md-3 col-lg-3">
            <div class="contact-info">
              <section>
                <strong>Contato</strong>
                <p>
                  (81) 3545.7198<br />
                  (81) 3543-2464<br />
                  (81) 99205-4028
                </p>
              </section>
    
              <section>
                <strong>Estamos no whatsaap</strong>
                <p>(81) 3545.7198</p>
              </section>
    
              <section>
                <strong>Onde estamos</strong>
                <p>Rua das Mangueiras, 23<br /> Umbura, Igarassu /PE<br /> CEP: 53625-577</p>
              </section>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <div class="newsletter">
              <form action="" method="post" class="form" id="form-news">
                <h2>Receba nossas novidades</h2>
                <input type="text" name="nome" placeholder="Nome" class="form-control" />
                <input type="email" name="email" placeholder="Email" class="form-control" />
                <button type="submit" class="btn-default">Assinar newsletter <i class="icon-arrow"></i></button>
              </form>
            </div>
          </div>
        </div>
        <p class="copyright">© ADELTEC, TODOS OS DIREITOS RESERVADOS</p>
      </div>
    </footer>
  
    <script src="js/lib/jquery.js"></script>
    <script src="js/lib/owl.carousel.min.js"></script>
    <script src="js/app/main.js"></script>
  </body>
</html>